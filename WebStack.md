# Webtechnologien

Wenn man Java für die Programmierung eines Servers für Web-Anwendungen nutzt, muss man sich mit einer Vielfalt von Technologien auseinander setzen.


## Protokolle

### HTTP

### Websockets

## Browser

### HTML

* Referenz zu den HTML-Elementen
  - https://www.w3schools.com/tags/default.asp

### CSS


### JavaScript und das DOM

* JavaScript-Tutorial
  - https://www.w3schools.com/js/default.asp
* JavaScript-Referenz zu JavaScript-Objekten, zum DOM und zu den HTML-Objekten
  - https://www.w3schools.com/jsref/

https://developer.mozilla.org/en-US/docs/Web/HTTP/Session
