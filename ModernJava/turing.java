import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.Random;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

/* Der Code ist ein Beispiel, der viele Eigenschaften für
   einen modernen Programmierstil zusammenbringt.
*/

enum Move { LEFT, RIGHT; }

class Transition {
    String fromState, toState;
    char read, write;
    Move move;
    private static Random random = new Random();
    public Transition(String fromState, char read,
                      char write, Move move, String toState) {
        Objects.requireNonNull(move);
        this.fromState = fromState;
        this.read = read;
        this.write = write;
        this.move = move;
        this.toState = toState;
    }
    @Override public String toString() {
        return String.format("(%s, %s) -> (%s, %s, %s)",
                             fromState, read, write, move, toState);
    }
    public static Optional<Transition> find(Collection<Transition> transitions, Predicate<Transition> predicate) {
        return transitions.stream()
                          .filter(predicate)
                          .reduce((t1, t2) -> random.nextInt(2) == 0 ? t1 : t2);
    }
    public static Predicate<Transition> matcher(String state, char read) {
        return t -> t.fromState.equals(state) && t.read == read;
    }
}

class Cell { // LinkedCell
    Cell left, right;
    char defaultValue, value;

    public Cell(char defaultValue) {
        value = this.defaultValue = defaultValue;
    }
    public Cell move(Move move) {
        Cell cell = null;
        switch(move) {
            case RIGHT:
                if (right != null) cell = right;
                else right = cell = new Cell(defaultValue);
                cell.left = (left == null && value == defaultValue) ? null : this;
                break;
            case LEFT:
                if (left != null) cell = left;
                else left = cell = new Cell(defaultValue);
                cell.right = (right == null && value == defaultValue) ? null : this;
                break;
        }
        return cell;
    }
    public Cell slide(int n, Move move) {
        if (n == 0) return this;
        return this.move(move).slide(n - 1, move);
    }
    public char read() {
        return value;
    }
    public Cell write(char value) {
        this.value = value; return this;
    }
    public Cell init(String init) {
        Cell cell = this;
        for(char c : init.toCharArray()) {
            cell = cell.write(c).move(Move.RIGHT);
        }
        return cell;
    }
    @Override public String toString() {
        String s = "";
        Cell c = this;
        while(c.left != null) c = c.left;
        do {
            s += (c == this ? "<" : " " ) + c.value + (c == this ? ">" : " ");
            c = c.right;
        } while (c != null);
        return s;
    }
}

class Env {
    private String state;
    private Cell cell;
    private int counter = 0;
    public Env(Cell cell, String state) {
        this.cell = cell;
        this.state = state;
    }
    public Env update(Cell cell, String state) {
        counter++;
        this.cell = cell;
        this.state = state;
        return this;
    }
    public String getState() { return state; }
    public Cell getCell() { return cell; }
    @Override public String toString() {
        return state + " : " + cell + " [" + counter + "]"; 
    }
}

class TM {
    final Collection<Transition> transitions;
    public TM(Collection<Transition> transitions) {
        this.transitions = transitions;
    }
    public Env step(Env env) {
        String state = env.getState();
        Cell  cell  = env.getCell(); 
        return Transition
                    .find(transitions, Transition.matcher(state, cell.read()))
                    .map(transition ->
                        env.update(cell.write(transition.write)
                                       .move(transition.move),
                                   transition.toState))
                    .orElse(null);
    }
    public Stream<Env> run(Env env) {
        return Stream.iterate(env, this::step).takeWhile(Objects::nonNull);
    }
}

Collection<Transition> r0 = List.of(
    new Transition("1",'1','1',Move.LEFT,"1"),
    new Transition("1",'0','1',Move.RIGHT,"2"),
    new Transition("2",'1','0',Move.RIGHT,"2"),
    new Transition("2",'0','1',Move.LEFT,"3"),
    new Transition("3",'0','0',Move.LEFT,"3"),
    new Transition("3",'1','1',Move.RIGHT,"4"),
    new Transition("4",'0','0',Move.LEFT,"H")
);

TM tm = new TM(r0);
String word = "01111110";
Cell cell = new Cell('_').init(word).slide(word.length(),Move.LEFT);
Env env = new Env(cell, "1");

Collection<Transition> trans1 = List.of(
    new Transition("1",'e','e',Move.RIGHT,"1"),
    new Transition("1",'1','1',Move.RIGHT,"1"),
    new Transition("1",'-','-',Move.RIGHT,"1"),
    new Transition("1",'=','e',Move.LEFT,"2"),
    new Transition("2",'1','=',Move.LEFT,"3"),
    new Transition("2",'-','e',Move.LEFT,"H"),
    new Transition("3",'1','1',Move.LEFT,"3"),
    new Transition("3",'-','-',Move.LEFT,"4"),
    new Transition("4",'e','e',Move.LEFT,"4"),
    new Transition("4",'1','e',Move.RIGHT,"1")   
);

TM tm1 = new TM(trans1);
String word1 = "111-1=";
Cell cell1 = new Cell('e').init(word1).slide(word1.length(),Move.LEFT);
Env env1 = new Env(cell1, "1");

/*
    List<T> result = rows.stream().filter(predicate)
                         .collect(Collectors.toList());
    return result.isEmpty() ? 
    Optional.empty() :
    Optional.of(result.get(rand.nextInt(result.size())));
 */


