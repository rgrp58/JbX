---
author: Dominikus Herzberg
---

# Scoop zur Installation von Software <!-- omit in toc -->

Um sich die Installation der benötigten Softwarepakete zu vereinfachen, empfehle ich [Scoop](https://scoop.sh/) für Windows und [Homebrew](https://brew.sh/index_de.html) für macOS, das auch nutzbar ist für Linux bzw. das Windows Subsystem for Linux. Scoop ist insbesondere gedacht für die Installation von Werkzeugen, die über die Kommandozeile genutzt werden. Graphische Anwendungen, wie z.B. einen Browser oder eine Entwicklungsumgebung, sollten Sie wie gewohnt von der Webseite zur Anwendung herunterladen und installieren.

- [In Kürze: Der Umgang mit Scoop](#in-k%C3%BCrze-der-umgang-mit-scoop)
- [Ein Beispiel: Die Installation von Java](#ein-beispiel-die-installation-von-java)


## In Kürze: Der Umgang mit Scoop

Der große Vorteil von Scoop liegt darin, dass Sie sich nicht um das Setzen von Umgebungsvariablen kümmern müssen (eine Herausforderung, an der man manchmal verzweifeln kann) und dass es sehr leicht ist, Pakete zu aktualisieren oder sie gar wieder zu löschen.

Scoop ist einfach zu bedienen. Öffnen Sie nach der Installation von Scoop eine Kommandozeile. Mit Drücken der `<Windows>`-Taste + `R` (für _run_) werden Sie nach dem Kommando zur Ausführung gefragt. Geben Sie `cmd` ein und die Kommandozeile öffnet sich. Wenn Sie nun `scoop` eingeben, bekommen Sie eine Kurzübersicht über die Scoop-Befehle. Das ist fast selbsterklärend, oder?

~~~ text
>scoop
Usage: scoop <command> [<args>]

Some useful commands are:

alias       Manage scoop aliases
bucket      Manage Scoop buckets
cache       Show or clear the download cache
checkup     Check for potential problems
cleanup     Cleanup apps by removing old versions
config      Get or set configuration values
create      Create a custom app manifest
depends     List dependencies for an app
export      Exports (an importable) list of installed apps
help        Show help for a command
home        Opens the app homepage
info        Display information about an app
install     Install apps
list        List installed apps
prefix      Returns the path to the specified app
reset       Reset an app to resolve conflicts
search      Search available apps
status      Show status and check for new app versions
uninstall   Uninstall an app
update      Update apps, or Scoop itself
virustotal  Look for app's hash on virustotal.com
which       Locate a shim/executable (similar to 'which' on Linux)


Type 'scoop help <command>' to get help for a specific command.
~~~

Die Informationen für die Installationspakete sind in öffentlich zugänglichen Git-Verzeichnissen abgelegt, die Scoop "Eimer" (_bucket_) nennt.

* https://github.com/lukesampson/scoop/tree/master/bucket, _main bucket_
* https://github.com/lukesampson/scoop-extras, _extra bucket_

Darüber hinaus stehen weitere _Buckets_ zur Verfügung, wie z.B. für die Installation von Java. Sie können sich die Liste aller _Buckets_ (die _bucket list_) entweder im [Git-Repo](https://github.com/lukesampson/scoop/blob/master/buckets.json) anschauen oder Scoop um Auskunft bitten.

~~~
C:\Users\Dominikus>scoop bucket known
extras
versions
nightlies
nirsoft
php
nerd-fonts
nonportable
java
games
jetbrains
~~~

Man kann sich, wenn man im Zweifel über die Herkunft eines Softwarepackets ist, die json-Dateien in den jeweiligen _Buckets_ anschauen, um festzustellen, aus welchen Quellen Scoop die Software installiert und welche Befehle zum Einsatz kommen.

Ähnlich wie Scoop ist Homebrew organisiert, die Liste aller Pakete ist einzusehen unter

* https://formulae.brew.sh/formula/

## Ein Beispiel: Die Installation von Java

Beispielhaft zeige ich Ihnen, wie Sie Java mit Hilfe von Scoop installieren.

Da das Java Development Kit (JDK) nicht im _main bucket_ zur Verfügung steht, mache ich mich auf die Suche, wo das JDK zu finden ist. Es gibt den Java-Bucket, und der bietet gleich eine Vielzahl an JDKs an.

~~~
C:\Users\Dominikus>scoop search jdk
Results from other known buckets...
(add them using 'scoop bucket add <name>')

'java' bucket:
    bucket/adoptopenjdk-hotspot-jre
    bucket/adoptopenjdk-hotspot
    bucket/adoptopenjdk-openj9-jre
    bucket/adoptopenjdk-openj9
    bucket/ojdkbuild
    bucket/ojdkbuild10
    bucket/ojdkbuild11
    bucket/ojdkbuild8
    bucket/ojdkbuild9
    bucket/openjdk
    bucket/openjdk10
    bucket/openjdk11
    bucket/openjdk12
    bucket/openjdk13
    bucket/openjdk7-unofficial
    bucket/openjdk9
    bucket/oraclejdk-lts
    bucket/oraclejdk
    bucket/oraclejdk11
    bucket/oraclejdk8-ea
    bucket/oraclejdk8
    bucket/oraclejdk8u
~~~

Fügen Sie den Java-Bucket zu Scoop hinzu.

~~~
C:\Users\Dominikus>scoop bucket add java
Checking repo... ok
The java bucket was added successfully.
~~~

Nun müssen Sie sich entscheiden, welche Installation für das JDK Sie wählen. Von welchem Anbieter und zu welcher Sprachversion von Java möchten Sie das Paket beziehen?

> Ich empfehle Ihnen das [OpenJDK](https://de.wikipedia.org/wiki/OpenJDK), da die restriktive Lizenzpolitik von Oracle Ihnen Steine in den Weg legt, sobald sich auch nur geringste kommerzielle Aspekte im Gebrauch des OracleJDK andeuten.

Mit Scoop ist die Installation rasch und unkompliziert erledigt.

~~~ shell
C:\Users\Dominikus>scoop install openjdk11
Installing 'openjdk11' (11.0.2-9) [64bit]
openjdk-11.0.2_windows-x64_bin.zip (178,7 MB) [=======================] 100%
Checking hash of openjdk-11.0.2_windows-x64_bin.zip ... ok.
Extracting openjdk-11.0.2_windows-x64_bin.zip ... done.
Linking ~\scoop\apps\openjdk11\current => ~\scoop\apps\openjdk11\11.0.2-9
'openjdk11' (11.0.2-9) was installed successfully!
~~~

Nur eines müssen Sie noch wissen: In Windows werden die Umgebungsvariablen in der aktuellen Kommandozeile leider nicht aktualisiert. Man muss eine neue Kommandozeile öffnen (siehe oben). Und dann können Sie leicht überprüfen, ob Java wie geplant bereitsteht.

~~~ shell
C:\Users\Dominikus>java --version
openjdk 11.0.2 2019-01-15
OpenJDK Runtime Environment 18.9 (build 11.0.2+9)
OpenJDK 64-Bit Server VM 18.9 (build 11.0.2+9, mixed mode)
~~~

Viel Erfolg mit der Installation weiterer Softwarepakete!

