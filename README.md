# Javalin by Example (JbX) <!-- omit in toc -->

In diesem Repository geht es rund um die Programmierung von Javalin-Anwendungen mit Java und um einen modernen Programmierstil mit Java.

Sie sind herzlich eingeladen, mit weiteren Code-Beispielen zu diesem Repository beizutragen. Und wenn Sie Fehler finden und Korrekturvorschläge haben, auch solche Beiträge sind wertvoll.

- [Übersicht zu Javalin-Beispielen](#%C3%BCbersicht-zu-javalin-beispielen)
- [Aufgaben](#aufgaben)
- [Übersicht zu Beispielen mit einem modernen Java-Programmierstil](#%C3%BCbersicht-zu-beispielen-mit-einem-modernen-java-programmierstil)
- [Benötigte Software](#ben%C3%B6tigte-software)
- [Wie man mit dem Git-Repository `JbX` arbeitet](#wie-man-mit-dem-git-repository-jbx-arbeitet)

## Übersicht zu Javalin-Beispielen

- [Ein einfaches Beispiel mit HTML-Forms](HTMLFormsExample) -- Das ist ein guter Einstieg!
- [Ein Spielfeld für Tic-Tac-Toe](TicTacToe.Spielfeld) -- Hier lernen Sie vieles, was Sie brauchen werden.

## Aufgaben

* [Aufgabe 1: Taschenrechner-UI](Aufgaben/Aufgabe1.md)
* [Aufgabe 2: Währungsrechner](Aufgaben/Aufgabe2.md)
* [Aufgabe 3: Zahleneingabe für Taschenrechner](Aufgaben/Aufgabe3.md)

## Übersicht zu Beispielen mit einem modernen Java-Programmierstil

Hier finden Sie Beispiele zum Nachschlagen und Lernen. Zu jedem Beispiel sind die wesentlichen Programmkonstrukte gelistet und der ein oder andere Hinweis. Die Beispiele sind vollkommen unabhängig von Javalin.

* [Primzahl-Würfel](ModernJava/PrimzahlWuerfel.md): `List.of`, `flatMap`, `filter`, `count`, `map`, `Stream<Integer>`, `IntStream`, `Arrays.stream()`, mehrfache Nutzung eines Streams
* [Turing-Maschine](ModernJava/turing.java): `Objects.requireNonNull`, `Objects.nonNull`, `Predicate<>`, `filter`, `find`, `iterate`, `map`, `reduce`, `takeWhile`,  `Optional<>`, `orElse`, `List.of`, 

## Benötigte Software

Sie müssen, wenn Sie in Java programmieren und Javalin-Anwendungen schreiben wollen, einige Werkzeuge auf Ihrem Rechner installiert haben. Hier die Auswahl der Programme, die ich auf meinem Rechner installiert habe:

* Das aktuelle JDK (Java Development Kit)
* Einen Programmeditor wie [Visual Studio Code](https://code.visualstudio.com/) (VSC); auch der Editor [Atom](http://atom.io) ist beliebt
  - Die Erweiterung "Java Extension Pack" von Microsoft sollten Sie unbedingt für VSC installieren
* Die Build-Werkzeuge gradle und maven
* Das Versionsverwaltungsprogramm git
* Das Kommandozeilenwerkzeug curl

Für die Installation dieser Programme (mit Ausnahme von Visual Studio Code) empfehle ich Ihnen auf Windows, Scoop zu nehmen. Problemloser geht es nicht.

* [Installation von Software mit Scoop](Scoop.md)

## Wie man mit dem Git-Repository `JbX` arbeitet

Dieses Repository _Javalin by Example_ (JbX) ist innerhalb der THM für Sie zugänglich unter der Adresse:

https://git.thm.de/dhzb87/JbX

Sie können sich das Repository (Repo) als gepackte Datei herunterladen. Aber am besten nutzen Sie git, indem Sie das Repo `clonen`.

    git clone https://git.thm.de/dhzb87/JbX.git

Wenn Sie das Dateien in Ihrem lokalen `JbX`-Verzeichnis mit dem Repo auf den neuesten Stand bringen wollen, _pullen_ (herunterziehen) sich die aktuellste Fassung.

    git pull

Wechsel Sie in das Verzeichnis, das Sie interessiert. Liegt dort eine Datei namens `build.gradle`, können Sie die Anwendung übersetzen und ausführen mit

    gradle run
---

Quellennachweis für das "Logo": [Pixabay](https://pixabay.com/de/illustrations/geschenke-hintergrund-bunte-box-1913987/) (Zugriff: 21.3.2019)